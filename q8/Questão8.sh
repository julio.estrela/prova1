#!/bin/bash

# Pedir ao usuário para digitar 3 nomes de arquivo separados por espaço
echo "Digite o nome de três arquivos separados por espaço:"
read f

# Extrair os nomes dos arquivos
a=$(echo "$f" | cut -d" " -f1)
b=$(echo "$f" | cut -d" " -f2)
c=$(echo "$f" | cut -d" " -f3)

# Contar o número de linhas de cada arquivo e somar os resultados
s=$(( $(wc -l < "$a") + $(wc -l < "$b") + $(wc -l < "$c") ))

# Imprimir a soma dos números de linhas
echo "> $s"


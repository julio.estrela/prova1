#!/bin/bash

# Frases da poesia
p="A vida é bela"
q="O amor é eterno"
w="A natureza é sábia"
e="A arte é inspiradora"
r="A amizade é um tesouro"

# Imprimir cada frase com pausas aleatórias e cores diferentes
echo -e "\033[3$((RANDOM%6 + 1))m$p"
sleep $(echo "scale=2; 0.5 + $RANDOM / 32768 * 2.5" | bc)
echo -e "\033[3$((RANDOM%6 + 1))m$q"
sleep $(echo "scale=2; 0.5 + $RANDOM / 32768 * 2.5" | bc)
echo -e "\033[3$((RANDOM%6 + 1))m$w"
sleep $(echo "scale=2; 0.5 + $RANDOM / 32768 * 2.5" | bc)
echo -e "\033[3$((RANDOM%6 + 1))m$e"
sleep $(echo "scale=2; 0.5 + $RANDOM / 32768 * 2.5" | bc)
echo -e "\033[3$((RANDOM%6 + 1))m$r"

# Resetar cor para padrão no final
echo -e "\033[0m"

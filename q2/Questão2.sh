#!/bin/bash

# Definir as variáveis a, b e c
a="/tmp"
b="/etc"
c="/bin"

# Contar a quantidade de arquivos e diretórios em /tmp
ls -l $a | grep "^-" | wc -l | xargs echo -n
echo -n " arquivos e "
ls -l $a | grep "^d" | wc -l | xargs echo -n
echo " diretórios."

# Contar a quantidade de arquivos e diretórios em /etc
ls -l $b | grep "^-" | wc -l | xargs echo -n
echo -n " arquivos e "
ls -l $b | grep "^d" | wc -l | xargs echo -n
echo " diretórios."

# Contar a quantidade de arquivos e diretórios em /bin
ls -l $c | grep "^-" | wc -l | xargs echo -n
echo -n " arquivos e "
ls -l $c | grep "^d" | wc -l | xargs echo -n
echo " diretórios."


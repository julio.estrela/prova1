#!/bin/bash

# Formas de criar variáveis no Bash:

# 1. Atribuição direta:
a="valor1"

# 2. Atribuição usando comandos:
b=$(pwd)
c=`date`

# 3. Atribuição com leitura de entrada do usuário:
read -p "Digite o valor da variável: " d

# 4. Atribuição usando parâmetros de linha de comando:
e=$1

# Diferença entre pedir ao usuário e receber como parâmetro de linha de comando:
echo "Diferença entre pedir ao usuário e receber como parâmetro de linha de comando:"
echo "Ao pedir ao usuário, o script espera que o usuário insira o valor durante a execução."
echo "Ao receber como parâmetro de linha de comando, o valor é passado como argumento quando o script é chamado."

# Exemplos de variáveis automáticas:

# Variáveis de posição:
echo "Nome do script: $0"
echo "Primeiro parâmetro: $1"
echo "Segundo parâmetro: $2"

# Outras variáveis automáticas:
echo "Número de parâmetros: $#"
echo "Lista de parâmetros: $@"
echo "Último comando de saída: $?"

# Informações sobre o ambiente:
echo "ID do processo: $$"
echo "Nome do usuário: $USER"
echo "Diretório atual: $PWD"

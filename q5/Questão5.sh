#!/bin/bash

# Pedir ao usuário para digitar 4 nomes
echo "Por favor, digite 4 nomes:"
read a
read b
read c
read d

# Obter data atual
d=$(date +"%d-%m-%Y")

# Criar diretórios e arquivos README.md para cada nome
mkdir "$a" && echo -e "# $a\nData: $data" > "$a/README.md"
mkdir "$b" && echo -e "# $b\nData: $data" > "$b/README.md"
mkdir "$c" && echo -e "# $c\nData: $data" > "$c/README.md"
mkdir "$d" && echo -e "# $d\nData: $data" > "$d/README.md"

echo "Diretórios e arquivos README.md criados com sucesso."

#!/bin/bash

# Pedir ao usuário para digitar um número inteiro
echo "Por favor, digite um número inteiro:"
read x

# Calcular o valor de y
y=$((x * x * x + 3))

# Imprimir os valores de x e y
echo "O valor de x é: $x"
echo "O valor de y é: $y"

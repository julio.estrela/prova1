#!/bin/bash

# Informações do processador
echo "### Processador ###"
cat /proc/cpuinfo

# Informações da memória
echo -e "\n### Memória ###"
cat /proc/meminfo | grep -E "MemTotal|MemFree"

# Informações dos dispositivos PCI
echo -e "\n### Dispositivos PCI ###"
lspci

# Informações dos dispositivos USB
echo -e "\n### Dispositivos USB ###"
lsusb

# Informações dos discos
echo -e "\n### Discos ###"
lsblk

# Informações da placa-mãe
echo -e "\n### Placa-mãe ###"
dmidecode -t baseboard | grep -E "Manufacturer:|Product:"

# Informações do BIOS
echo -e "\n### BIOS ###"
dmidecode -t bios | grep -E "Vendor:|Version:|Release Date:"

# Informações do sistema
echo -e "\n### Informações do Sistema ###"
uname -a

# Informações do kernel
echo -e "\n### Kernel ###"
cat /proc/version

#!/bin/bash

# Obter o dia da semana (em formato numérico) para hoje
d=$(date +%u)

# Calcular o número de dias até a próxima quarta-feira
q=$(( (3 - d + 7) % 7 ))

# Calcular a data da próxima quarta-feira
a=$(date -d "+$q days" "+%Y-%m-%d")

# Calcular a data da quarta-feira seguinte
b=$(date -d "$a + 7 days" "+%d-%m-%Y")

# Exibir as datas das duas próximas quartas-feiras
echo "A próxima quarta-feira será em: $a"
echo "A quarta-feira seguinte será em: $b"

